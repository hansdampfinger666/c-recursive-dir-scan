#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <wordexp.h>

#define MAX_DIR_LEN 256

typedef enum {
  FTUNKNOWN,
  FTMUSIC,
  FTDOCUMENT,
  FTVIDEO,
  FTNUM,
} FileType;

typedef struct {
  FileType type;
  const char *dir;
} DirMap;

#define NUM_DIR_MAPS 3

const DirMap dir_maps[3] = {
    {FTMUSIC, "~/Music/"},
    {FTDOCUMENT, "~/Documents/"},
    {FTVIDEO, "~/Videos/"},
};

#define NO_MUSIC_EXTS 3

const char music_exts[NO_MUSIC_EXTS][10] = {
    "mp3",
    "wav",
    "flac",
};

#define NO_DOC_EXTS 1

const char doc_exts[NO_DOC_EXTS][10] = {
    "run",
};

#define NUM_CHILDREN 10
typedef struct DirScan DirScan;

struct DirScan {
  char dir[MAX_DIR_LEN];
  int level;
  int sums_file_types[NUM_DIR_MAPS];
  DirScan *parent;
  size_t num_children;
  size_t child_cap;
  DirScan **children;
};

int add_child(DirScan *parent, DirScan *child) {

  if (parent->num_children >= parent->child_cap) {

    size_t new_cap = parent->child_cap * 2;
    DirScan **new_children =
        (DirScan **)realloc(parent->children, new_cap * sizeof(DirScan *));

    if (new_children == NULL) {
      fprintf(stderr, "Memory allocation failed in %s\n", __func__);
      return EXIT_FAILURE;
    }

    parent->children = new_children;
    parent->child_cap = new_cap;
  }

  parent->children[parent->num_children++] = child;
  return EXIT_SUCCESS;
}

void print_dir_structure(const DirScan *dir_scan) {

  printf("Dir: %s\n", dir_scan->dir);

  for (int i = 0; i < dir_scan->num_children; ++i) {
    printf("Dir: %s\n", dir_scan->children[i]->dir);
    print_dir_structure(dir_scan);
  }
}

void get_target_dir(const FileType file_type, char *result,
                    size_t result_size) {

  for (size_t i = 0; i < FTNUM; ++i) {

    if (file_type == dir_maps[i].type) {
      strncpy(result, dir_maps[i].dir, result_size - 1);
      result[result_size - 1] = '\0';
      return;
    }
  }
  result[0] = '\0';
}

void expand_tilde(char *path, char *result, size_t result_size) {

  if (strchr(path, '~') == NULL) {
    return;
  }

  wordexp_t expansion;
  wordexp(path, &expansion, 0);

  if (path == result) {
    memset(result, '\0', result_size);
  }

  for (char **i = expansion.we_wordv; *i != NULL; i++) {
    strcat(result, *i);
  }
}

FileType get_file_type(const char *file) {

  const char *dot = strrchr(file, '.');

  if (!dot || dot == file) {
    return FTUNKNOWN;
  }
  dot++;

  for (int i = 0; i < NO_MUSIC_EXTS; ++i) {

    if (strcmp(dot, music_exts[i]) == 0) {
      printf("Ext: %s (%d)\n", music_exts[i], FTMUSIC);
      return FTMUSIC;
    }
  }
  for (int i = 0; i < NO_DOC_EXTS; ++i) {

    if (strcmp(dot, doc_exts[i]) == 0) {
      // printf("Ext: %s (%d)\n", doc_exts[i], FTDOCUMENT);
      return FTDOCUMENT;
    }
  }

  return FTUNKNOWN;
}

bool is_dot_dir(char *path) {

  if (strcmp(path, ".") == 0 || strcmp(path, "..") == 0) {
    return true;
  }
  return false;
}

int move_files(DirScan *dir_scan) {

  expand_tilde(dir_scan->dir, dir_scan->dir, MAX_DIR_LEN);

  DIR *dir_stream;

  if ((dir_stream = opendir(dir_scan->dir)) == NULL) {
    fprintf(stderr, "Error opening dir %s\n", dir_scan->dir);
    return EXIT_FAILURE;
  }

  struct dirent *dir_ent;
  char path[MAX_DIR_LEN];

  while ((dir_ent = readdir(dir_stream)) != NULL) {

    if (is_dot_dir(dir_ent->d_name)) {
      continue;
    }

    sprintf(path, "%s/%s", dir_scan->dir, dir_ent->d_name);
    struct stat stat_buf;

    if (stat(path, &stat_buf) == -1) {
      printf("Dir (unable to stat dir entry): %s\n", dir_ent->d_name);
      continue;
    }

    if (S_ISDIR(stat_buf.st_mode)) {

      DirScan *sub_dir_scan = (DirScan *)malloc(sizeof(DirScan));

      if (!sub_dir_scan) {
        fprintf(stderr, "Memory allocation failed in %s\n", __func__);
        return EXIT_FAILURE;
      }

      memset(sub_dir_scan->dir, '\0', MAX_DIR_LEN);

      sub_dir_scan->level = dir_scan->level + 1;
      sub_dir_scan->parent = dir_scan;
      strcpy(sub_dir_scan->dir, dir_scan->dir);
      strcat(sub_dir_scan->dir, "/");
      strcat(sub_dir_scan->dir, dir_ent->d_name);

      if (add_child(dir_scan, sub_dir_scan) != 0) {
        return EXIT_FAILURE;
      }

      for (int i = 0; i < sub_dir_scan->level - 1; ++i) {
        printf("  ");
      }

      printf("|-");
      printf("Dir: %s\n", sub_dir_scan->dir);

      if (move_files((sub_dir_scan)) != 0) {
        printf("Trying to copy: %s\n", dir_scan->dir);
        printf("and the subdir: %s\n", dir_ent->d_name);
        printf("Source len: %li\n", strlen(dir_scan->dir));
        printf("Dest len: %li\n", strlen(sub_dir_scan->dir));
        fprintf(stderr, "Recursion error in %s\n", __func__);
        return EXIT_FAILURE;
      }

    } else {

      for (int i = 0; i < dir_scan->level; ++i) {
        printf("  ");
      }

      printf("|-");
      printf("File: %s\n", dir_ent->d_name);

      const FileType type = get_file_type(dir_ent->d_name);
      char target_dir[MAX_DIR_LEN] = {0};
      get_target_dir(type, target_dir, sizeof(target_dir));

      if (strcmp(target_dir, "") != 0) {
        expand_tilde(target_dir, target_dir, MAX_DIR_LEN);
        // printf("Target dir should be: %s\n", target_dir);
      }
    }
  }

  closedir(dir_stream);
  return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {

  printf("Dir: %s\n", "~/Documents");
  DirScan dir_scan = {.dir = "~/Downloads"};

  if (move_files((&dir_scan)) != 0) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
